package com.skydev.torneo.domain.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "jugador")
@Data
public class Jugador {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idJugador;
    private String nombre;
}
