package com.skydev.torneo.domain.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "partido")
@Data
public class Partido {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     private int idPartido ;
     private int   idTipo_torneo ;
     private int   idJugador1;
     private int   idJugador2;
     private int   resultado1;
     private int   resultado2;
}
