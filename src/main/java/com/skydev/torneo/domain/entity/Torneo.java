package com.skydev.torneo.domain.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
@Entity
@Table(name = "torneo")
@Data
public class Torneo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTorneo;
    @Column(name = "idTipo_torneo")
    private int idTipoTorneo;
    private int idPartido;
    private LocalDateTime fecha;
}
