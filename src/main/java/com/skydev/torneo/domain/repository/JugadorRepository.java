package com.skydev.torneo.domain.repository;

import com.skydev.torneo.domain.entity.Jugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
@Repository
public interface JugadorRepository extends JpaRepository<Jugador, Integer> {
}
