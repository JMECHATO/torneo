package com.skydev.torneo.domain.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_tornei")
@Data
public class TipoTorneo {
    private int idTipo_torneo;
    private String descripcion;
}
